# A Matlab Wrapper for DenseCRF 

This repo is the folder for MATLAB wrapper of the DenseCRF code. This tool has been used in many of my personal projects.

### Citing

If you find it useful in your research, please consider citing:

    @article{koltun2011efficient,
      title={Efficient Inference in Fully Connected CRFs with Gaussian Edge Potentials},
      author={Koltun, Philipp Kr{\"a}henb{\"u}hl Vladlen},
      booktitle={NIPS}
      year={2011}
    }

## Code directory
- src
- test

## Dependencies
- [EIGEN](included)
- [OpenBLAS](http://www.openblas.net/)
- [LIBJPEG](http://libjpeg.sourceforge.net/)
- [MPICH](https://www.mpich.org/)
- [MATLAB](https://www.mathworks.com/products/matlab.html)

## Compiling 
- Install dependencies
- Make sure the path of `mex.h` is properly included in your environment variables.
- Modify `TINYOBJLOADER_HOME` and `NVIDIA_LIB` in `Makefile`
- Run `make all`

## Running 
- Here is an example of how to use DenseCRF to improve segmentation performance. 
- Download the data from here: [data](http://www.cs.toronto.edu/~slwang/mexdensecrf_data.tar.gz)
- Modify the `densecrf_path` on line 3 of `demo_test_densecrf.m`
- Run `demo_test_densecrf.m` for test. 