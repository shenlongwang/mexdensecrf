#include "mex.h" 
#include "densecrf.h"
#include <cstdio>
#include <cmath>
#include "ppm.h"
#include "common.h"

const float GT_PROB = 0.8; // 0.6 for simpler texture;


MatrixXf computeUnary(const VectorXs & lbl, int M){
	const float u_energy = -log(1.0 / M);
	const float n_energy = -log((1.0 - GT_PROB) / (M - 1));
	const float p_energy = -log(GT_PROB);
	MatrixXf r(M, lbl.rows());
	r.fill(u_energy);
	//printf("%d %d %d \n",im[0],im[1],im[2]);
	for (int k = 0; k<lbl.rows(); k++){
		// Set the energy
		if (lbl[k] >= 0){
			r.col(k).fill(n_energy);
			r(lbl[k], k) = p_energy;
		}
	}
	return r;
}

MatrixXf getUnaryBinary(const float* im_unary, int M, int im_size){
	const float u_energy = -log(1.0 / 2.0);
	MatrixXf r(M, im_size);
	r.fill(u_energy);
	//printf("%d %d %d \n",im[0],im[1],im[2]);
	for (int k = 0; k<im_size; k++){
		// Set the energy
		r.col(k).fill(-log(1.0f - im_unary[k]));
		r(1, k) = -log(im_unary[k]) + 0.01;
	}
	return r;
}

MatrixXf getUnaryMultiClass(const float* im_unary, int M, int im_size) {
	const float u_energy = -log(1.0 / M);
	const float average_prob = 1.0 / M;
	MatrixXf r(M, im_size);
	r.fill(u_energy);
	//printf("%d %d %d \n",im[0],im[1],im[2]);
	for (int k = 0; k<im_size; k++) {
		// Set the energy
		for (int l = 0; l < M; l++ )
			if (im_unary[l*im_size + k] > average_prob)
				r(l, k) = -log(im_unary[l*im_size+k]);
	}
	return r;
}

float* exportProb(const MatrixXf & Q, int W, int H, int M) {
	float* r = new float[W*H*M];
	int im_size = W*H;
	for (int k = 0; k<im_size; k++) {
		// Set the energy
		for (int l = 0; l < M; l++)
			r[l*im_size + k] = Q(l, k);
	}
	//printf("%d %d %d \n",r[0],r[1],r[2]);
	return r;
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	if (nrhs != 8) {
		mexErrMsgIdAndTxt("MATLAB:mexgetarray:minrhs", 
			"Eight parameters are required: image, depth, im_unary, transfer_unary, parameters, width, height, number of labels.");
	}

	unsigned char* im = (unsigned char*)mxGetPr(prhs[0]);
	unsigned char* depth = (unsigned char*)mxGetPr(prhs[1]);
	float* im_unary = (float*) mxGetPr(prhs[2]);
	float* transfer_unary = (float*)mxGetPr(prhs[3]);
	float* params = (float *) mxGetPr(prhs[4]);
	const int W = (int)mxGetScalar(prhs[5]);
	const int H = (int)mxGetScalar(prhs[6]);
	const int M = (int)mxGetScalar(prhs[7]);
	float std_g = params[0]; // std_gaussian
	float std_b = params[1]; // std_bilateral
	float std_c = params[2]; // std_color
	float std_d = params[3]; // std_depth
	float weight_g = params[4]; //weight_gaussian
	float weight_b = params[5]; //weight_bilateral
	float weight_d = params[6]; //weight_depth
	float weight_t = params[7]; //weight_transfer
	int n_iteration = (int) params[8]; // n_iterations

	mexPrintf("Params are: %f %f %f %f %f %d \n", std_g, std_b, std_c, std_d, weight_g, n_iteration);

	MatrixXf unary = getUnaryMultiClass(im_unary, M, W*H);
	MatrixXf unary_transfer = getUnaryMultiClass(transfer_unary, M, W*H);
	unary = unary.cast<float>() + weight_t * unary_transfer.cast<float>();
	DenseCRF2D crf(W, H, M);
	crf.setUnaryEnergy(unary);
	crf.addPairwiseGaussian(std_g, std_g, new PottsCompatibility(weight_g));
	crf.addPairwiseDepthBilateral(std_b, std_b, std_d, depth, new PottsCompatibility(weight_b));
	crf.addPairwiseBilateral(std_b, std_b, std_c, std_c, std_c, im, new PottsCompatibility(weight_d));
	MatrixXf map = crf.inference(n_iteration);
	//// Store the result
	float *res = exportProb(map, W, H, M);
	plhs[0] = mxCreateNumericMatrix(W*H, M, mxSINGLE_CLASS, mxREAL);
	memcpy((float*)mxGetData(plhs[0]), res, sizeof(float)*W*H*M);
	delete[] res;
}
