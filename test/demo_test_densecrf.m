clear;
load densecrf1;
densecrf_path = '/ais/gobi4/slwang/localization/code/dcrf/mexdensecrf'; % Change to your own path
addpath(densecrf_path);

disparity = uint8(depth);
im = uint8(im_left*255);
im_unary = single(score_left);
transfer_unary = single(score_left_transfer);
% params = [3.0, 50.0, 10.0, 15.0, ...
%     0.5, 0.5, 20, 0.6, 5];
params = [3.0, 80.0, 10.0, 2.0, ...
    1.0, 3.0, 5.0, 0.6, 5];
% std_gaussian, std_bilateral, std_color, std_depth
% weight_gaussian, weight_bilateral, weight_depth, weight_transfer, n_iterations

[W, H, M] = size(score_left);
tic; score_map = mexdensecrf(im, disparity, im_unary, transfer_unary,...
    single(params), int32(W), int32(H), int32(M));
toc
score_map = reshape(score_map, W, H, M);
[~, max_id] = max(score_map, [], 3);
[~, max_unary] = max(score_left, [], 3);
[~, max_transfer] = max(score_left_transfer, [], 3);

imagesc([max_id, max_unary, max_transfer]); axis equal; axis off;

imwrite(uint8([max_id]/12*255), jet(256), 'max_densecrf.png');
imwrite(uint8([max_unary]/12*255), jet(256), 'max_left.png');
imwrite(uint8([max_transfer]/12*255), jet(256), 'max_transfer.png');
imwrite(uint8(im_left*255), jet(256), 'im_left.png');
imwrite(uint8(im_right*255), jet(256), 'im_right.png');
imwrite(disparity, jet(256), 'disparity.png');
